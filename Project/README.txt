
Instructions for reproducing the results.

Note: Make sure you have a good GPU before running the scripts. On my Titan Xp the training took approximately 2 hours.
For the required python packages see ./Code/requirements.txt
The pretrained models were created with pytorch 1.1.0, and might not give the same result when evaluating with a different version.
When training from scratch, the version does not matter.

Data:
	Download data from here: https://www.dropbox.com/sh/ihuoipmjn2ap0iy/AADB1K-Q_xNvtdUg-eQ6VSCpa?dl=0
	Unzip wisc_images.zip and news_images.zip into the Data directory (such that you have ./Data/wisc_images/ and ./Data/news_images/)
	Copy the pretained models into the Models directory (such that you have ./Models/vgg19_000010_10_augOn_refine_noFlip_512x512/vgg19_000010_10_augOn_refine_noFlip_512x512_014.pwf and ./Models/vgg19_000010_10_augOn_refine_noFlip_noColorJitter_512x512/vgg19_000010_10_augOn_refine_noFlip_noColorJitter_512x512_013.pwf)

Navigate to ./Code

Data preprocessing:
	Tweak the parameter in ./run_split_data.py as desired (rando split or grouping).
	Execute ./run_split_data.py to create training, validation and test sets. The specified directory is expected to contain a directory called 'all'. For the different splits more directories next to 'all' are created.

Training:
	Execute ./run_training.py to perform the training. Without changing anything, the configuration for the best performing model should be executed.

Evaluation:
	Because the training is not deterministic, the best checkpoint might be different from what is currently set in the code. Therefore you have to manually find the checkpoint index with lowest loss and set at the bottom of ./run_evaluation.py for cfg.checkpoint_to_load.
	Then simply execute ./run_evaluation.py to evaluate the model on the training, validation and test set or new unlabelled data and create the images, plots and confusion matrix results. 

Image classification and object detection:
	To run the image classification (1000 classes) and object detection execute ./run_imagenet_classification_eval.py and ./run_rcnn_detection_eval.py respectively.

Results:
	Additional image results can also be found in the dropbox folder.



Google Doc with notes:
https://docs.google.com/document/d/1FdxSJAZHIyOEiaIm_b-X_SONb5Xc_pyM7hfAGRMTqSE/edit
