import os
import cv2
import torch
import torchvision
from torchvision.transforms import functional as F
from models.classifiers.imagenet_classes import IMAGENET_CLASSES

from config.config import *
from data.dataset import *

import json
import collections
import pandas as pd

from utils import logging

logger = logging.get_logger()



# ---------------------------------------------------------------------------------------------------

# model = torchvision.models.vgg19_bn(pretrained=True)
# model = torchvision.models.vgg19(pretrained=True)
model = torchvision.models.resnet50(pretrained=True)


model.eval()


config = ClassificationConfig()
which_gpu = 0

# ---------------------------------------------------------------------------------------------------

if torch.cuda.is_available():
    device = torch.device('cuda:{}'.format(which_gpu))
    logger.info("Running on GPU {} ({})".format(which_gpu, torch.cuda.get_device_name(which_gpu)))
else:
    device = torch.device('cpu')
    logger.warning("Running on CPU. Training will be very slow!")

# ---------------------------------------------------------------------------------------------------

# split = 'training'
# split = 'validation'
# split = 'test'
split = 'cnn_images'
# split = 'fox_images'
# split = 'msnbc_images'

if split in ['training', 'validation', 'test']:
    dataset = AdsPartyDataset(config, split)
else:
    directory = '../Data/news_images/' + split + '/'
    dataset = UnlabelledDataset(config, directory)

dataloader = torch.utils.data.DataLoader(dataset, batch_size=1, num_workers=0, shuffle=False)
logger.info("Num images: {}".format(len(dataloader)))

model.to(device)

# ---------------------------------------------------------------------------------------------------

logger.info("")
logger.info("---------------------------------------------------------------")
logger.info("")

# ---------------------------------------------------------------------------------------------------

mean = torch.as_tensor([0.485, 0.456, 0.406])
std = torch.as_tensor([0.229, 0.224, 0.225])

threshold = 0.3
democrat_classes = collections.Counter()
republican_classes = collections.Counter()

# dict with columnes for filename, party, all imagenet classes
column_names = ['filename', 'party']
for i in range(1000):
    column_names.append(IMAGENET_CLASSES[i])

extracted_data = {}
for name in column_names:
    extracted_data[name] = []



for idx, (inputs, targets) in enumerate(dataloader):
    logger.info("Index: {}".format(idx))

    input_img = inputs['image'].to(device)
    target_party = targets['party'].to(device)

    # input_img = input_img.unsqueeze(0)
    outputs = model(input_img)
    outputs = torch.nn.functional.softmax(outputs, dim=1)

    extracted_data['filename'].append(inputs['filename'][0])
    party = 'unknown'
    if target_party.item() == 0:
        party = 'democrat'
    if target_party.item() == 1:
        party = 'republican'
    extracted_data['party'].append(party)
    for i in range(1000):
        extracted_data[IMAGENET_CLASSES[i]].append(outputs[0, i].item())


    probabilities, indices = torch.topk(outputs, 10, 1)

    probabilities = probabilities.cpu().squeeze()
    indices = indices.cpu().squeeze()

    logger.info("Party: {}".format('democrat' if target_party.item() == 0 else 'republican'))
    for i in range(10):
        logger.info("{:>5.2f}%, {} ({})".format(probabilities[i].item() * 100, IMAGENET_CLASSES[indices[i].item()], indices[i].item()))
        if probabilities[i].item() > threshold:
            if target_party.item() == 0:
                democrat_classes.update({ IMAGENET_CLASSES[indices[i].item()] : 1 })
            else:
                republican_classes.update({ IMAGENET_CLASSES[indices[i].item()] : 1 })


    input_img = input_img.cpu().squeeze()
    input_img = F.normalize(input_img, -mean / std, 1 / std) # -> (tensor - (-mean / std)) / ( 1 / std) = tensor * std + mean

    img = input_img.permute(1, 2, 0).numpy()
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    cv2.imshow('image', img)

    logger.info(democrat_classes)
    logger.info(republican_classes)
    logger.info("")
    logger.info("---------------------------------------------------------------")
    logger.info("")

    cv2.waitKey(1)



df = pd.DataFrame(extracted_data, columns=column_names)
df.to_pickle("./classification_{}.pkl".format(split))

with open("./classification_democrat.txt", 'w') as f:
    f.write(json.dumps(dict(democrat_classes.most_common()), indent=4, separators=(',', ' : ')))

with open("./classification_republican.txt", 'w') as f:
    f.write(json.dumps(dict(republican_classes.most_common()), indent=4, separators=(',', ' : ')))