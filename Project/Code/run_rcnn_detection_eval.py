import os
import cv2
import torch
import torchvision
from torchvision.transforms import functional as F
from models.classifiers.coco_instance_categories import COCO_INSTANCE_CATEGORY_NAMES

from config.config import *
from data.dataset import *

import json
import collections
import pandas as pd

from utils import logging

logger = logging.get_logger()



# ---------------------------------------------------------------------------------------------------

# image in range 0-1 (not noralized)
model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True)

model.eval()


config = ObjectDetectionConfig()
which_gpu = 0

# ---------------------------------------------------------------------------------------------------

if torch.cuda.is_available():
    device = torch.device('cuda:{}'.format(which_gpu))
    logger.info("Running on GPU {} ({})".format(which_gpu, torch.cuda.get_device_name(which_gpu)))
else:
    device = torch.device('cpu')
    logger.warning("Running on CPU. Training will be very slow!")

# ---------------------------------------------------------------------------------------------------

# split = 'training'
# split = 'validation'
split = 'test'
# split = 'cnn_images'
# split = 'fox_images'
# split = 'msnbc_images'

if split in ['training', 'validation', 'test']:
    dataset = AdsPartyDataset(config, split)
else:
    directory = '../Data/news_images/' + split + '/'
    dataset = UnlabelledDataset(config, directory)
dataloader = torch.utils.data.DataLoader(dataset, batch_size=1, num_workers=0, shuffle=True)

model.to(device)

# ---------------------------------------------------------------------------------------------------

logger.info("")
logger.info("---------------------------------------------------------------")
logger.info("")

# ---------------------------------------------------------------------------------------------------

mean = torch.as_tensor([0.485, 0.456, 0.406])
std = torch.as_tensor([0.229, 0.224, 0.225])

threshold = 0.3
democrat_classes = collections.Counter()
republican_classes = collections.Counter()


# dict with columnes for filename, party, all imagenet classes
column_names = ['filename', 'party']
column_names.extend(COCO_INSTANCE_CATEGORY_NAMES)

extracted_data = {}
for name in column_names:
    extracted_data[name] = []


for idx, (inputs, targets) in enumerate(dataloader):
    logger.info("Index: {}".format(idx))

    img_tensor = inputs['image']
    input_img = img_tensor.clone()
    input_img = input_img.to(device)
    target_party = targets['party'].to(device)

    # input_img = input_img.unsqueeze(0)
    outputs = model(input_img)

    # input_img = input_img.cpu().squeeze()
    # img_tensor = F.normalize(img_tensor, -mean / std, 1 / std) # -> (tensor - (-mean / std)) / ( 1 / std) = tensor * std + mean
    img = img_tensor.squeeze().permute(1, 2, 0).numpy()
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

    outputs = outputs[0]
    boxes = outputs['boxes'].cpu() # N x 4
    labels = outputs['labels'].cpu() # N
    scores = outputs['scores'].cpu() # N

    all_scores = [0 for _ in range(len(COCO_INSTANCE_CATEGORY_NAMES))]
    for label, score in zip(labels, scores):
        all_scores[label.item()] = max(score.item(), all_scores[label.item()]) # only record the highest probability of each detected object

    extracted_data['filename'].append(inputs['filename'][0])
    party = 'unknown'
    if target_party.item() == 0:
        party = 'democrat'
    if target_party.item() == 1:
        party = 'republican'
    extracted_data['party'].append(party)
    for name, score in zip(COCO_INSTANCE_CATEGORY_NAMES, all_scores):
        extracted_data[name].append(score)


    logger.info("Party: {}".format('democrat' if target_party.item() == 0 else 'republican'))
    for i in range(len(labels)):
        if scores[i].item() > threshold:
            cv2.rectangle(img, (boxes[i][0], boxes[i][1]), (boxes[i][2], boxes[i][3]), color=(0, 255, 0), thickness=2) # Draw Rectangle with the coordinates
            cv2.putText(img, COCO_INSTANCE_CATEGORY_NAMES[labels[i].item()], (boxes[i][0], boxes[i][1]),  cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0), thickness=1) # Write the prediction class

            if target_party.item() == 0:
                democrat_classes.update({ COCO_INSTANCE_CATEGORY_NAMES[labels[i].item()] : 1 })
            else:
                republican_classes.update({ COCO_INSTANCE_CATEGORY_NAMES[labels[i].item()] : 1 })

        logger.info("{:>5.2f}%, {} ({})".format(scores[i].item() * 100, COCO_INSTANCE_CATEGORY_NAMES[labels[i].item()], labels[i].item()))


    cv2.imshow('image', img)

    logger.info(democrat_classes)
    logger.info(republican_classes)
    logger.info("")
    logger.info("---------------------------------------------------------------")
    logger.info("")

    cv2.waitKey(1)

    if idx > 1:
        break

df = pd.DataFrame(extracted_data, columns=column_names)
df.to_pickle("./detection_{}.pkl".format(split))

with open("./detection_democrat.txt", 'w') as f:
    f.write(json.dumps(dict(democrat_classes.most_common()), indent=4, separators=(',', ' : ')))

with open("./detection_republican.txt", 'w') as f:
    f.write(json.dumps(dict(republican_classes.most_common()), indent=4, separators=(',', ' : ')))