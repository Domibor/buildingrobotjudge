'''
    run evaluation
    test set, new data
'''


from config import config
from models.classifiers.ImgToParty import *
from data.dataset import *
import numpy as np
import random
from utils import logging, stopwatch, checkpoint, utils
from datetime import datetime # summary writer
from tensorboardX import SummaryWriter
import os
from torchvision.transforms import functional as F
import torch
import cv2
from matplotlib import pyplot as plt
import pandas as pd

logger = logging.get_logger()

which_gpu = 0


def evaluation(config, data_set_mode = 'test'):

    logger.info("")
    logger.info("Model name: {}".format(config.model_name))
    logger.info("")

    # ---------------------------------------------------------------------------------------------------

    if torch.cuda.is_available():
        device = torch.device('cuda:{}'.format(which_gpu))
        logger.info("Running on GPU {} ({})".format(which_gpu, torch.cuda.get_device_name(which_gpu)))
    else:
        device = torch.device('cpu')
        logger.warning("Running on CPU. Training will be very slow!")

    # ---------------------------------------------------------------------------------------------------

    logger.info("")
    suffix = "{}_{}".format(config.model_name, config.checkpoint_to_load)
    if data_set_mode == 'training':
        dataset = AdsPartyDataset(config, 'training')
        suffix += '_train'
        logger.info("Evaluating Training Set")
    elif data_set_mode == 'validation':
        dataset = AdsPartyDataset(config, 'validation')
        suffix += '_val'
        logger.info("Evaluating Validation Set")
    elif data_set_mode == 'test':
        dataset = AdsPartyDataset(config, 'test')
        suffix += '_test'
        logger.info("Evaluating Test Set")
    else:
        # otherwise assume data_set_mode is a directory
        dataset = UnlabelledDataset(config, data_set_mode)
        name = os.path.basename(os.path.normpath(data_set_mode))
        suffix += '_unlabelled_' + name
        logger.info("Evaluating Unlabelled Set: {}".format(name))
        data_set_mode = name
    logger.info("")

    # create dataloaders
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=1, shuffle=False, num_workers=0)

    logger.info("Dataset: {} samples.".format(len(dataloader)))

    # ---------------------------------------------------------------------------------------------------

    model = ImageParty(config)
    model.to(device)

    # ---------------------------------------------------------------------------------------------------

    logger.info("")
    logger.info("---------------------------------------------------------------")
    logger.info("")
    epoch_stopwatch = stopwatch.Stopwatch()

    # ---------------------------------------------------------------------------------------------------

    model_dir = os.path.join("../Models/", config.model_name)
    checkpoint.load_checkpoint(model_dir, config.model_name, config.checkpoint_to_load, device, model)

    # ---------------------------------------------------------------------------------------------------
    # validation
    model.eval()

    losses = logging.AverageMeter()
    accuracies = logging.AverageMeter()

    # positive = democrat = label 0
    stats = {
        'tp' : 0,
        'fp' : 0,
        'tn' : 0,
        'fn' : 0,
        'P_dem' : [],
        'P_rep' : [],
        'gt' : []
    }

    show_image = False

    # dict with columnes for filename, party, all imagenet classes
    column_names = ['filename', 'party', 'P(democrat)', 'P(republican)']
    extracted_data = {}
    for name in column_names:
        extracted_data[name] = []


    progress = logging.ProgressPrinter("Evaluation | Sample {step}/{max_step} ({percent:.1f}%) | Loss: {loss:.6f} | Acc: {acc:.2f}% | P(dem): {P_dem:>6.2f}% | P(rep): {P_rep:>6.2f}% | Correct: {correct} | ETA: {eta} | Total Time: {total_time}",
                                        max_step=len(dataloader), log_step=1)
    progress.start(loss=float("inf"), acc=0, P_dem=0, P_rep=0, correct=str(bool(False)).ljust(5))

    mean = torch.as_tensor([0.485, 0.456, 0.406])
    std = torch.as_tensor([0.229, 0.224, 0.225])

    with torch.no_grad():
        for idx, (inputs, targets) in enumerate(dataloader, 1):
            input_img = inputs['image'].to(device)
            target_party = targets['party'].to(device)

            outputs = model(input_img)
            outputs = torch.nn.functional.softmax(outputs, dim=1)
            _, predicted = torch.max(outputs, 1)

            P_dem = outputs[0][0].item()
            P_rep = outputs[0][1].item()
            pred = predicted[0]

            extracted_data['filename'].append(inputs['filename'][0])
            party = 'unknown'
            if target_party.item() == 0:
                party = 'democrat'
            if target_party.item() == 1:
                party = 'republican'
            extracted_data['party'].append(party)
            extracted_data['P(democrat)'].append(P_dem)
            extracted_data['P(republican)'].append(P_rep)



            # DEBUG
            # P_dem = random.uniform(0, 1)
            # P_rep = 1 - P_dem
            # pred = 0 if P_dem > P_rep else 1

            gt = target_party[0].item()

            if pred == 0:
                if target_party[0] == 0:
                    stats['tp'] += 1
                else:
                    stats['fp'] += 1
            else: # 1
                if target_party[0] == 1:
                    stats['tn'] += 1
                else:
                    stats['fn'] += 1

            stats['P_dem'].append( P_dem )
            stats['P_rep'].append( P_rep )
            stats['gt'].append( gt )

            correct = (pred == gt)

            # DEBUG
            # accuracies.update(correct)

            accuracies.update(correct.item())
            progress.step(loss=losses.average, acc=accuracies.average * 100, P_dem=P_dem * 100, P_rep=P_rep * 100, correct=str(bool(correct)).ljust(5))

            # logger.info("Probabilities: {}, Predicted: {}, GT: {}".format((outputs[0][0].item(), outputs[0][1].item()), predicted[0].item(), target_party[0].item()))


            if show_image:
                logger.info("P(dem): {}, P(rep): {}".format(P_dem, P_rep))
                input_img = input_img.cpu().squeeze()
                input_img = F.normalize(input_img, -mean / std, 1 / std) # -> (tensor - (-mean / std)) / ( 1 / std) = tensor * std + mean

                img = input_img.permute(1, 2, 0).numpy()
                img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
                cv2.imshow('image', img)

                cv2.waitKey(0)

        logger.info("Average accuracy: {}".format(accuracies.average))

        # --------------------------------------------------------------------------------------------------------------
        # confusion matrix

        total_count = len(dataloader)
        stats['P_tp'] = stats['tp'] / total_count
        stats['P_fp'] = stats['fp'] / total_count
        stats['P_tn'] = stats['tn'] / total_count
        stats['P_fn'] = stats['fn'] / total_count
        stats['precision'] = stats['tp'] / (stats['tp'] + stats['fp'])
        stats['recall'] = stats['tp'] / (stats['tp'] + stats['fn'])
        if stats['precision'] * stats['recall'] > 0:
            stats['F1'] = 2 * (stats['precision'] * stats['recall']) / (stats['precision'] + stats['recall'])
        else:
            stats['F1'] = 0
        logger.info("Confusion matrix: TP {P_tp} ({tp}), FP {P_fp} ({fp}), TN {P_tn} ({tn}), FN {P_fn} ({fn})".format(**stats))
        logger.info("Precision: {precision}, Recall: {recall}, F1 {F1}".format(**stats))

        # --------------------------------------------------------------------------------------------------------------
        # for each decile find corresponding indices

        deciles = [dec / 10 for dec in range(1, 11)]
        stats['bins_dem'] = np.digitize(stats['P_dem'], deciles, right=True)
        stats['bins_rep'] = np.digitize(stats['P_rep'], deciles, right=True)

        stats['deciles_all_idxs_dem'] = [[] for _ in range(len(deciles))]
        stats['deciles_all_idxs_rep'] = [[] for _ in range(len(deciles))]

        for idx, (bin_dem, bin_rep) in enumerate(zip(stats['bins_dem'], stats['bins_rep'])):
            stats['deciles_all_idxs_dem'][bin_dem].append(idx)
            stats['deciles_all_idxs_rep'][bin_rep].append(idx)


        num_imgs = 10
        stats['deciles_idxs_dem'] = [[] for _ in range(len(deciles))]
        stats['deciles_idxs_rep'] = [[] for _ in range(len(deciles))]
        for idx in range(len(deciles)):
            indices = stats['deciles_all_idxs_dem'][idx]
            if len(indices) > num_imgs:
                indices = random.sample(indices, k=num_imgs)
                stats['deciles_idxs_dem'][idx] = indices

            indices = stats['deciles_all_idxs_rep'][idx]
            if len(indices) > num_imgs:
                indices = random.sample(indices, k=num_imgs)
                stats['deciles_idxs_rep'][idx] = indices


        def create_deciles_img(indices, min_width, shape):
            imgs_deciles = []
            for img_idxs in indices:
                imgs = []
                random.shuffle(img_idxs)
                for idx in img_idxs:
                    inputs, targets = dataset[idx]
                    input_img = inputs['image']
                    input_img = F.normalize(input_img, -mean / std, 1 / std) # -> (tensor - (-mean / std)) / ( 1 / std) = tensor * std + mean

                    img = input_img.permute(1, 2, 0).numpy()
                    imgs.append(img)


                if len(imgs) < min_width:
                    imgs.extend([np.zeros(shape)] * (min_width - len(imgs)))
                imgs_deciles.append(np.hstack(imgs))

            img = np.vstack(imgs_deciles)
            return img.astype(np.float32)

        img_dem = create_deciles_img(stats['deciles_idxs_dem'], num_imgs, (config.input_dim, config.input_dim, 3))
        img_rep = create_deciles_img(stats['deciles_idxs_rep'], num_imgs, (config.input_dim, config.input_dim, 3))
        img_dem = cv2.cvtColor(img_dem, cv2.COLOR_RGB2BGR)
        img_rep = cv2.cvtColor(img_rep, cv2.COLOR_RGB2BGR)
        cv2.imwrite("./preds_dem_{}.png".format(suffix), (img_dem * 255).astype(np.uint8))
        cv2.imwrite("./preds_rep_{}.png".format(suffix), (img_rep * 255).astype(np.uint8))
        cv2.namedWindow('img_dem', cv2.WINDOW_NORMAL)
        cv2.namedWindow('img_rep', cv2.WINDOW_NORMAL)
        cv2.imshow('img_dem', img_dem)
        cv2.imshow('img_rep', img_rep)
        # cv2.waitKey(0)

        # --------------------------------------------------------------------------------------------------------------
        # predicted probabilities

        plt.hist(stats['P_dem'], bins = deciles)
        plt.title("Probabilities (democrats)")
        # plt.show()
        plt.savefig('probabilities_dem_{}.png'.format(suffix))

        plt.clf()

        plt.hist(stats['P_rep'], bins = deciles)
        plt.title("Probabilities (republican)")
        # plt.show()
        plt.savefig('probabilities_rep_{}.png'.format(suffix))

        plt.clf()

        # ---------------------------------------------------------------------------------------------------


        df = pd.DataFrame(extracted_data, columns=column_names)
        df.to_pickle("./predictions_{}.pkl".format(data_set_mode))

    logger.info("")
    logger.info("Evaluation finished")
    logger.info("")
    logger.info("---------------------------------------------------------------")
    logger.info("")
    logger.info("")


if __name__ == "__main__": # without that we get 'broken pipe' error with multiple workers
    logger.init_logfile('evaluation')
    logger.info("===============================================================")
    logger.info("Evaluation")
    logger.info("===============================================================")
    logger.info("")


    cfg = config.Config()
    cfg.augmentation = False
    cfg.backbone = 'vgg19'

    # ---------------------------------------------------------------
    # uncomment these two lines for evaluating model with geometric and pixel augmentation
    cfg.model_name_suffix = "augOn_refine_noFlip_512x512"
    cfg.checkpoint_to_load = 14
    # ---------------------------------------------------------------

    # ---------------------------------------------------------------
    # uncomment these two lines for evaluating model with only geometric augmentation
    # cfg.model_name_suffix = "augOn_refine_noFlip_noColorJitter_512x512"
    # cfg.checkpoint_to_load = 13
    # ---------------------------------------------------------------

    # ---------------------------------------------------------------
    # DEBUG
    # dfpred = pd.read_pickle('./predictions_test.pkl')
    # dfpred['repub'] = dfpred['party'] == 'republican'
    # dfpred['predicted_repub'] = dfpred['P(republican)'] > 0.5
    # print((dfpred['repub'] == dfpred['predicted_repub']).mean())
    # ---------------------------------------------------------------

    # cfg.model_name_suffix = "augOn_refine"
    # cfg.checkpoint_to_load = -1


    # observation: much more FP than FN -> why? more falsely predicted as democrat than republican?

    logger.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    # evaluation(cfg, 'training')
    # evaluation(cfg, 'validation')
    evaluation(cfg, 'test')
    evaluation(cfg, '../Data/news_images/cnn_images/')
    evaluation(cfg, '../Data/news_images/fox_images/')
    evaluation(cfg, '../Data/news_images/msnbc_images/')
    logger.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
