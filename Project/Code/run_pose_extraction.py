'''
    extract poses from all images
    save pose information to json
'''

from utils import logging

logger = logging.get_logger()

# ===================================================================================
from config.config import *
# ===================================================================================

import os
from data import annotation as ann
import cv2

# TODO: setup model, dataset, forward pass, write to file
def pose_extraction():
    
    for party in parties:
        for year in years:
            curr_dir = os.path.join(image_root_dir, party, str(year))
            video_dirs = [video_dir for video_dir in os.listdir(curr_dir)]
            for vid_idx, video_dir in enumerate(video_dirs, 1):
                annotation_files = [annotaion for annotaion in os.listdir(video_dir) if video_dir.endswith('.json')]

                for annotation_file in annotation_files:
                    annotation = ann.Annotation(filename=annotation_file)
                    
                    image = cv2.imread(os.path.join(video_dir, annotation.image_filename))

                    # TODO: forward pass
                    # TODO: inference

                    annotation.pose = ann.Pose()

                    annotation.save(annotation_file)

                
                basename, _ = os.path.splitext(video_file)
                image_directory = os.path.join(image_root_dir, party, str(year), basename)

if __name__ == "__main__": # without that we get 'broken pipe' error with multiple workers
    logger.init_logfile('pose_extraction')
    logger.info("===============================================================")
    logger.info("Pose Extraction")
    logger.info("===============================================================")
    logger.info("")

    pose_extraction()