'''
    subsample videos
    create json for each frame
    add label (partisanship) to json
'''

from utils import logging
from preprocessing import split_videoframes

logger = logging.get_logger()

# ===================================================================================
from config.config import *
# ===================================================================================

if __name__ == "__main__": # without that we get 'broken pipe' error with multiple workers
    logger.init_logfile('video_preprocessing')
    logger.info("===============================================================")
    logger.info("Video Preprocessing")
    logger.info("===============================================================")
    logger.info("")

    split_videoframes.create_annotated_images(video_root_dir, parties, years, image_root_dir, step_size, image_shape)