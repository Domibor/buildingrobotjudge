import torch
from torchvision import transforms
import os
from config import config as cfg
from data import annotation as ann
import cv2
import numpy as np
from PIL import Image

class PartisanshipDataset(torch.utils.data.dataset.Dataset):
    def __init__(self, parties, years):
        self.json_files = []
        for party in parties:
            for year in years:
                curr_dir = os.path.join(cfg.image_root_dir, party, str(year))
                video_dirs = [video_dir for video_dir in os.listdir(curr_dir)]
                for vid_idx, video_dir in enumerate(video_dirs, 1):
                    annotation_files = [{ 'dir' : video_dir, 'ann' : annotation_file }
                                        for annotation_file in os.listdir(video_dir) if video_dir.endswith('.json')]

                    self.json_files.extend(annotation_files)


    def __len__(self):
        return len(self.json_files)

    def __getitem__(self, index):

        directory = self.json_files[index]['dir']
        ann_file = self.json_files[index]['ann']

        annotation = ann.Annotation(filename=os.path.join(directory, ann_file))
        image = cv2.imread(os.path.join(directory, annotation.image_filename))

        # TODO: image to tensor
        # TODO: annotation stuff to tensor

        inputs = {}
        outputs = {}

        return inputs, outputs


class AdsPartyDataset(torch.utils.data.dataset.Dataset):
    def __init__(self, config, split=None):
        '''
            split : training, validation, test, None
        '''
        self.config = config

        self.augmentation = False
        self.data = []
        endings = ('png', 'jpg', 'jpeg')

        directories = []
        if split == 'training':
            directories = self.config.training_directories
            self.augmentation = self.config.augmentation
        elif split == 'validation':
            directories = self.config.validation_directories
        elif split == 'test':
            directories = self.config.test_directories
        else:
            directories.append(self.config.training_directories)
            directories.append(self.config.validation_directories)
            directories.append(self.config.test_directories)

        for directory in directories:
            for file in os.listdir(directory):
                if file.endswith(endings):
                    party = file.split("_")[0]
                    if party.isdigit():
                        party = int(party)
                        if party in (1, 2):
                            self.data.append((directory, file, party))

        if self.config.resize_mode not in ['pad', 'crop']:
            self.config.resize_mode = 'pad'

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        directory, img_path, party = self.data[index]
        img = cv2.imread(os.path.join(directory, img_path)) # BGR, HxWxC
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img_tensor = transforms.ToTensor()(img)

        shape = img.shape[:2]

        if self.config.resize_mode == 'pad':
            # resize larger dimension
            dim_scale = max(shape[0], shape[1])
        elif self.config.resize_mode == 'crop':
            # resize smaller dimension
            dim_scale = min(shape[0], shape[1])

        # resize using the dim_scale
        new_shape = (int(shape[0] / dim_scale * self.config.input_dim), int(shape[1] / dim_scale * self.config.input_dim))
        pad = int(abs(new_shape[0] - new_shape[1]) / 2)



        trafos = []
        if self.augmentation:
            trafos.extend([
                transforms.ToPILImage(),
                # transforms.ColorJitter(brightness=0.2, contrast=0.1, saturation=0.1, hue=0.1),
                # transforms.RandomAffine(40, translate=(0.2, 0.2), scale=(0.6, 1.4), shear=10, resample=False, fillcolor=0),
                transforms.ColorJitter(brightness=0.1, contrast=0.1, saturation=0.1, hue=0.1),
                transforms.RandomAffine(20, translate=(0.1, 0.1), scale=(0.7, 1.3), shear=Image.BILINEAR, resample=False, fillcolor=0),
                # transforms.RandomHorizontalFlip(p=0.5),
                transforms.ToTensor()
            ])

        trafos.extend([
            transforms.ToPILImage(),
            # transforms.Resize(new_shape),
            # transforms.Pad(pad),
            transforms.Resize(self.config.input_dim),
            transforms.CenterCrop(self.config.input_dim), # crop around center to get square input
            transforms.ToTensor()
        ])

        if self.config.normalize_input:
            trafos.append( transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]) )

        trafo = transforms.Compose(trafos)

        input_tensor = trafo(img_tensor)

        input_dict = { 'image' : input_tensor, "filename" : img_path }
        output_dict = { 'party' : torch.tensor(party - 1) }

        return input_dict, output_dict


class UnlabelledDataset(torch.utils.data.dataset.Dataset):
    def __init__(self, config, directory):
        '''
            split : training, validation, test, None
        '''
        self.config = config

        self.data = []
        endings = ('png', 'jpg', 'jpeg')

        for file in os.listdir(directory):
            if file.endswith(endings):
                self.data.append((directory, file))

        if self.config.resize_mode not in ['pad', 'crop']:
            self.config.resize_mode = 'pad'

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        directory, img_path = self.data[index]
        img = cv2.imread(os.path.join(directory, img_path)) # BGR, HxWxC
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img_tensor = transforms.ToTensor()(img)

        shape = img.shape[:2]

        if self.config.resize_mode == 'pad':
            # resize larger dimension
            dim_scale = max(shape[0], shape[1])
        elif self.config.resize_mode == 'crop':
            # resize smaller dimension
            dim_scale = min(shape[0], shape[1])

        # resize using the dim_scale
        new_shape = (int(shape[0] / dim_scale * self.config.input_dim), int(shape[1] / dim_scale * self.config.input_dim))
        pad = int(abs(new_shape[0] - new_shape[1]) / 2)


        trafos = []
        trafos.extend([
            transforms.ToPILImage(),
            # transforms.Resize(new_shape),
            # transforms.Pad(pad),
            transforms.Resize(self.config.input_dim),
            transforms.CenterCrop(self.config.input_dim), # crop around center to get square input
            transforms.ToTensor()
        ])

        if self.config.normalize_input:
            trafos.append( transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]) )

        trafo = transforms.Compose(trafos)

        input_tensor = trafo(img_tensor)

        input_dict = { 'image' : input_tensor, "filename" : img_path  }
        output_dict = { 'party' : torch.tensor(-1) } # use -1 as indicator for no label

        return input_dict, output_dict