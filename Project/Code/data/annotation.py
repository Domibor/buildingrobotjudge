'''
    data annotation
'''

from utils import utils
import json


# class labels
DEMOCRAT = 0
REPUBLICAN = 1
NONE = 2

def get_class_label(parisanship):
    if parisanship.upper() == "DEMOCRAT":
        return DEMOCRAT
    if parisanship.upper() == "REPUBLICAN":
        return DEMOCRAT
    return NONE

class Keypoint(object):
    def __init__(self):
        self.position = (0, 0)
        
    def from_dict(self, dict):
        pass

    def to_dict(self):
        return { }

class Pose(object):
    def __init__(self):
        self.keypoints = []
        pass

    def from_dict(self, dict):
        pass

    def to_dict(self):
        return { }
        
class Annotation(object):
    def  __init__(self, image_filename=None, class_label=None, pose=None, filename=None):
        self.image_filename = image_filename
        self.class_label = class_label
        self.pose = pose
        if filename is not None:
            self.load(filename)

    def load(self, filename):
        with open(filename, 'r') as json_file:
            self.from_dict(json.load(json_file))

    def save(self, filename):
        directory, basename, extension = utils.split_path(filename)
        filename = utils.make_path((directory, basename, '.json'))
            
        utils.make_dirs_checked(directory)
        with open(filename, 'w') as file:
            file.write(json.dumps(self.to_dict(), indent=4, separators=(',', ' : ')))

    def from_dict(self, dict):
        self.image_filename = dict['image_filename']
        self.class_label = dict['class_label']
        self.pose = Pose(dict['pose'])

    def to_dict(self):
        return {
            'image_filename' : self.image_filename,
            'class_label' : self.class_label,
            'pose' : self.pose.to_dict() if self.pose is not None else None
        }
        
    def __repr__(self):
        ''' Pretty print (json string) '''
        return json.dumps(self.to_dict(), indent=4)