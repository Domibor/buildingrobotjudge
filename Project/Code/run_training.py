'''
    train the classifier
'''


from config import config
from models.classifiers.ImgToParty import *
from optimizer import adabound
from data.dataset import *
import numpy as np
from utils import logging, stopwatch, checkpoint, utils
from datetime import datetime # summary writer
from tensorboardX import SummaryWriter
import os

logger = logging.get_logger()

num_train_workers = 8
num_val_workers = 4
which_gpu = 0


def training(config):

    logger.info("")
    logger.info("Model name: {}".format(config.model_name))
    logger.info("")

    # ---------------------------------------------------------------------------------------------------

    if torch.cuda.is_available():
        device = torch.device('cuda:{}'.format(which_gpu))
        logger.info("Running on GPU {} ({})".format(which_gpu, torch.cuda.get_device_name(which_gpu)))
    else: 
        device = torch.device('cpu')
        logger.warning("Running on CPU. Training will be very slow!")

    # ---------------------------------------------------------------------------------------------------
    
    training_dataset = AdsPartyDataset(config, 'training')
    validation_dataset = AdsPartyDataset(config, 'validation')

    # create dataloaders
    trainloader = torch.utils.data.DataLoader(training_dataset, batch_size=config.batch_size, shuffle=True, num_workers=num_train_workers)
    valloader = torch.utils.data.DataLoader(validation_dataset, batch_size=config.batch_size, shuffle=True, num_workers=num_val_workers)
    
    logger.info("Training set: {} batches ({} samples). Validation set: {} batches ({} samples).".format(
                len(trainloader), len(trainloader.sampler), len(valloader), len(valloader.sampler)))

    # ---------------------------------------------------------------------------------------------------

    # Debug stuff for testing dataset class
    if False:
        from torchvision.transforms import functional as F
        mean = torch.as_tensor([0.485, 0.456, 0.406])
        std = torch.as_tensor([0.229, 0.224, 0.225])

        debug_loader = torch.utils.data.DataLoader(training_dataset, batch_size=1, num_workers=0)
        for batch_index, (inputs, targets) in enumerate(debug_loader, 1):
            logger.info("Party: {}".format('democrat' if targets['party'].item() == 0 else 'republican'))    

            input_img = inputs['image'].squeeze()
            input_img = F.normalize(input_img, -mean / std, 1 / std) # -> (tensor - (-mean / std)) / ( 1 / std) = tensor * std + mean

            img = input_img.permute(1, 2, 0).numpy()
            img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
            cv2.imshow('image', img)
        
            cv2.waitKey(0)


    # ---------------------------------------------------------------------------------------------------

    model = ImageParty(config)    
    model.to(device)
    total_num_params = sum(p.numel() for p in model.parameters())
    num_trainable_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
    logger.info("Number of trainable parameters: {} (total number of parameters: {})".format(num_trainable_params, total_num_params))

    model_dir = os.path.join("../Models/", config.model_name)
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)

    # ---------------------------------------------------------------------------------------------------
    
    optimizer = torch.optim.Adam(model.parameters(), lr=config.initial_lr)
    # optimizer = torch.optim.Adam(model.parameters(), lr=config.initial_lr, weight_decay=1e-5)
    # optimizer = torch.optim.SGD(model.parameters(), lr=config.initial_lr, momentum=0.9)
    # optimizer = adabound.AdaBound(model.parameters(), lr=config.initial_lr, final_lr=0.1)
    if config.scheduler == 'step':
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, config.lr_decay_step_size, config.lr_decay_gamma)    
    
    # optimizer = torch.optim.SGD(model.parameters(), lr=config.initial_lr * 2, momentum=0.9) # start with slightly larger lr
    elif config.scheduler == 'cosine_annealing_warm_restarts':
    # cosine annealing initial_lr to eta_min over T_i epochs (initially T_i = T_0), then restart, then over T_i = T_i * T_mult, repeat
        scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(optimizer, T_0=config.lr_decay_step_size, T_mult=1, eta_min=config.initial_lr * 0.01)    

    else:
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 1000000, 1)    

    criterion = nn.CrossEntropyLoss()

    # ------------------------------------------------------------------------------------------------------------
    # setup summary writer

    date_and_time = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    logdir = os.path.join('../Summary/runs', date_and_time + "__" + config.model_name) # for general logging
    utils.make_dirs_checked(logdir)

    summary_writer = SummaryWriter(logdir=logdir)       
    # dummy_input = torch.zeros(1, 3, config.input_dim, config.input_dim).to(device)
    # summary_writer.add_graph(model, dummy_input)
    
    # ---------------------------------------------------------------------------------------------------
    
    logger.info("")
    logger.info("---------------------------------------------------------------")
    logger.info("")
    epoch_stopwatch = stopwatch.Stopwatch()
    
    # ---------------------------------------------------------------------------------------------------

    start_epoch = 1
    global_step = 0


    if config.checkpoint_to_load is not None:
        model_to_load_dir = os.path.join("../Models/", config.model_to_load_name)
        last_start_epoch, last_global_step = checkpoint.load_checkpoint(model_to_load_dir, config.model_to_load_name, config.checkpoint_to_load,
                                                                        device, model, 
                                                                        optimizer if config.load_optimizer_state else None,
                                                                        scheduler if config.load_lr_scheduler_state else None)
        start_epoch = last_start_epoch + 1 if last_start_epoch is not None else start_epoch # the checkpoint finished with this epoch -> add 1 to start with next
        global_step = last_global_step + 1 if last_global_step is not None else global_step # the checkpoint finished with this step -> add 1 to start with next
        
    for epoch in range(start_epoch, start_epoch + config.num_epochs): # do num_epochs on top of what we already have
        epoch_stopwatch.tic()

        logger.info("[Epoch {} (Step {})]".format(epoch, global_step))

        scheduler.step()
            
        current_lr = optimizer.param_groups[0]['lr'] # assumes just 1 param group
        summary_writer.add_scalar("hyperparameter/learning_rate", current_lr, global_step)
        summary_writer.add_scalar("other/epoch", epoch, global_step)
        
        # ---------------------------------------------------------------------------------------------------
        # training
        model.train()
        
        data_time = logging.AverageMeter() # data loading
        batch_time = logging.AverageMeter() # batch processing
        losses = logging.AverageMeter()
        accuracies = logging.AverageMeter()
        batch_stopwatch = stopwatch.Stopwatch() # data loading and batch processing, not using synchronize_cuda for performance (does not need to be super accurate)


        progress = logging.ProgressPrinter("    Training | Batch {step}/{max_step} ({percent:.1f}%) | Loss: {loss:.6f} | Acc: {acc:.2f}% | Data Time: {data_time:.6f}s | Batch Time: {batch_time:.6f}s | ETA: {eta} | Total Time: {total_time}",
                                           max_step=len(trainloader), log_step=1)
        progress.start(loss=float("inf"), acc=0, data_time=0, batch_time=0)

        batch_stopwatch.tic()

        for batch_index, (inputs, targets) in enumerate(trainloader, 1):
            data_time.update(batch_stopwatch.toc())

            input_img = inputs['image'].to(device)
            target_party = targets['party'].to(device)
            
            optimizer.zero_grad()
            outputs = model(input_img)
            loss = criterion(outputs, target_party)

            loss.backward()                  
            optimizer.step()

            _, predicted = torch.max(outputs, 1)
            correct = (predicted == target_party).sum().item()

            losses.update(loss.item())
            accuracies.update(correct / target_party.size(0))
            batch_time.update(batch_stopwatch.toc())
            progress.step(loss=losses.average, acc=accuracies.average * 100, data_time=data_time.average, batch_time=batch_time.average)

            summary_writer.add_scalar("training/loss", loss.item(), global_step)
            summary_writer.add_scalar("training/accuracy", correct / target_party.size(0), global_step)

            global_step += 1
            
            batch_stopwatch.tic()


        progress.finish()

        # ---------------------------------------------------------------------------------------------------
        # validation
        model.eval()
        
        data_time = logging.AverageMeter() # data loading
        batch_time = logging.AverageMeter() # batch processing
        losses = logging.AverageMeter()
        accuracies = logging.AverageMeter()
        batch_stopwatch = stopwatch.Stopwatch() # data loading and batch processing, not using synchronize_cuda for performance (does not need to be super accurate)

        progress = logging.ProgressPrinter("    Validation | Batch {step}/{max_step} ({percent:.1f}%) | Loss: {loss:.6f} | Acc: {acc:.2f}% | Data Time: {data_time:.6f}s | Batch Time: {batch_time:.6f}s | ETA: {eta} | Total Time: {total_time}",
                                           max_step=len(valloader), log_step=1)
        progress.start(loss=float("inf"), acc=0, data_time=0, batch_time=0)

        batch_stopwatch.tic()

        with torch.no_grad():
            total_loss = 0
            for batch_index, (inputs, targets) in enumerate(valloader, 1): 
                data_time.update(batch_stopwatch.toc())

                input_img = inputs['image'].to(device)
                target_party = targets['party'].to(device)

                outputs = model(input_img)
                loss = criterion(outputs, target_party)
            
                _, predicted = torch.max(outputs, 1)
                correct = (predicted == target_party).sum().item()

                losses.update(loss.item())
                accuracies.update(correct / target_party.size(0))
                batch_time.update(batch_stopwatch.toc())
                progress.step(loss=losses.average, acc=accuracies.average * 100, data_time=data_time.average, batch_time=batch_time.average)
                
                batch_stopwatch.tic()

        summary_writer.add_scalar("validation/loss", losses.average, global_step)
        summary_writer.add_scalar("validation/accuracy", accuracies.average, global_step)
        
        progress.finish()

        # ---------------------------------------------------------------------------------------------------

        checkpoint_path = os.path.join(model_dir, "{}_{:03}.pwf".format(config.model_name, epoch))
        logger.info("    Saving checkpoint to \'{}\'".format(checkpoint_path))
        checkpoint.save_learned_model(checkpoint_path, model, epoch, global_step, optimizer, scheduler)
        
        # ---------------------------------------------------------------------------------------------------

        logger.info("    Epoch finished in {:.2f}sec".format(epoch_stopwatch.toc()))

        
    logger.info("")
    logger.info("Training finished")
    checkpoint_path = os.path.join(model_dir, "{}_{:03}.pwf".format(config.model_name, epoch))
    checkpoint.save_learned_model(checkpoint_path, model, epoch, global_step, optimizer, scheduler)
    logger.info("")
    logger.info("---------------------------------------------------------------")
    logger.info("")
    logger.info("")


if __name__ == "__main__": # without that we get 'broken pipe' error with multiple workers
    logger.init_logfile('training')
    logger.info("===============================================================")
    logger.info("Training")
    logger.info("===============================================================")
    logger.info("")


    # dynamically create configs with all combinations
    backbones = [
        # 'vgg11',
        # 'vgg11_bn',
        # 'vgg13',
        # 'vgg13_bn',
        # 'vgg16',
        # 'vgg16_bn',
        'vgg19',
        # 'vgg19_bn',
        # 'resnet18',
        # 'resnet34',
        # 'resnet50',
        # 'resnet101',
        # 'resnet152'
    ]
    learning_rates = [0.00001]
    batch_sizes = [10, 20, 30]
    augmentation = [False, True]

    configs = []

    cfg = config.Config()
    cfg.backbone = 'vgg19'

    # done
    # cfg.initial_lr = 0.00004
    # cfg.scheduler = None

    # now
    # cfg.initial_lr = 0.00004
    # cfg.scheduler = 'cosine_annealing_warm_restarts'

    # cfg.initial_lr = 0.00002
    # cfg.scheduler = None

    # cfg.initial_lr = 0.00002
    # cfg.scheduler = 'cosine_annealing_warm_restarts'

    cfg.initial_lr = 0.00001
    cfg.scheduler = None

    # cfg.initial_lr = 0.00001
    # cfg.scheduler = 'cosine_annealing_warm_restarts'

    cfg.lr_decay_step_size = 10
    cfg.lr_decay_gamma = 0.1
    cfg.num_epochs = 50
    cfg.augmentation = True
    cfg.refine_backbone_feature_extractor = True
    cfg.model_name_suffix = "augOn_refine_noFlip_512x512"
    cfg.checkpoint_to_load = None
    cfg.load_optimizer_state = True
    cfg.load_lr_scheduler_state = True
    configs.append(cfg)

    # for lr in learning_rates:
    #     for bb in backbones:
    #         cfg = config.Config()
    #         cfg.backbone = bb
    #         cfg.initial_lr = lr
    #         cfg.augmentation = True
    #         cfg.num_epochs = 25
    #         cfg.refine_backbone_feature_extractor = True
    #         cfg.model_name_suffix = "refine"
    #         cfg.checkpoint_to_load = -1
    #         cfg.load_optimizer_state = False
    #         cfg.load_lr_scheduler_state = False
    #         configs.append(cfg)

    for cfg in configs:
        logger.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        training(cfg)
        logger.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
