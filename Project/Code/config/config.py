
# # ======================================================================
# # directories

# video_root_dir = "../Data/Videos"
# parties = ["Democrat", "Republican"]
# years = list(range(2008, 2019 + 1))

# image_root_dir = "../Data/Images"

# # ======================================================================
# # preprocessing

# step_size = 10
# image_shape = 512

# # ======================================================================

class Config(object):
    def __init__(self):

        self.backbone = 'vgg11'
        self.load_pretrained_backbone = True        
        self.refine_backbone_feature_extractor = False        
        self.resize_mode = 'pad'
        self.normalize_input = True
        
        self.input_dim = 224
        self.augmentation = True

        self.training_directories = [
            "../Data/wisc_images/Storyboards - Governor/training/",
            "../Data/wisc_images/Storyboards - House/training/",
            "../Data/wisc_images/Storyboards - Senate/training/"
        ] # 80%
        self.validation_directories = [
            "../Data/wisc_images/Storyboards - Governor/validation/",
            "../Data/wisc_images/Storyboards - House/validation/",
            "../Data/wisc_images/Storyboards - Senate/validation/"
        ] # 10%
        self.test_directories = [
            "../Data/wisc_images/Storyboards - Governor/test/",
            "../Data/wisc_images/Storyboards - House/test/",
            "../Data/wisc_images/Storyboards - Senate/test/"
            # "../Data/wisc_images/test/"
        ] # 10%

        self.batch_size = 10
        self.initial_lr = 0.00001
        self.lr_decay_step_size = 1000
        self.lr_decay_gamma = 1
        self.scheduler = 'step'
        self.num_epochs = 50

        # self.model_name = "model_name"
        self.model_name_suffix = None
        self.checkpoint_to_load_model_name = None
        self.checkpoint_to_load = None
        self.load_optimizer_state = False
        self.load_lr_scheduler_state = False

    @property
    def model_name(self):
        name = "{backbone}_{lr}_{batch}".format(**{
            'backbone' : self.backbone,
            'lr' : format(self.initial_lr, 'f')[2:],
            'batch' : self.batch_size,
        })
        if self.model_name_suffix is not None:
            name += "_" + self.model_name_suffix
        return name

    @property
    def model_to_load_name(self):
        return self.checkpoint_to_load_model_name if self.checkpoint_to_load_model_name is not None else self.model_name

class ClassificationConfig(Config):
    def __init__(self):
        super().__init__()
        
        self.normalize_input = True
        self.input_dim = 224

class ObjectDetectionConfig(Config):
    def __init__(self):
        super().__init__()
        
        self.normalize_input = False
        self.input_dim = 800


# ======================================================================

class Config_VGG11(Config):
    def __init__(self):
        super().__init__()
        self.backbone = 'vgg11'

class Config_VGG11_BN(Config):
    def __init__(self):
        super().__init__()
        self.backbone = 'vgg11_bn'

class Config_VGG13(Config):
    def __init__(self):
        super().__init__()
        self.backbone = 'vgg13'

class Config_VGG13_BN(Config):
    def __init__(self):
        super().__init__()
        self.backbone = 'vgg13_bn'

class Config_VGG16(Config):
    def __init__(self):
        super().__init__()
        self.backbone = 'vgg16'

class Config_VGG16_BN(Config):
    def __init__(self):
        super().__init__()
        self.backbone = 'vgg16_bn'

class Config_VGG19(Config):
    def __init__(self):
        super().__init__()
        self.backbone = 'vgg19'

class Config_VGG19_BN(Config):
    def __init__(self):
        super().__init__()
        self.backbone = 'vgg19_bn'

class Config_Resnet18(Config):
    def __init__(self):
        super().__init__()
        self.backbone = 'resnet18'

class Config_Resnet34(Config):
    def __init__(self):
        super().__init__()
        self.backbone = 'resnet34'

class Config_Resnet50(Config):
    def __init__(self):
        super().__init__()
        self.backbone = 'resnet50'

class Config_Resnet101(Config):
    def __init__(self):
        super().__init__()
        self.backbone = 'resnet101'

class Config_Resnet152(Config):
    def __init__(self):
        super().__init__()
        self.backbone = 'resnet152'