
import os
from utils import utils, logging
import numpy as np
import shutil

logger = logging.get_logger()

def pop_lists_until_max_len(stack_of_lists, max_len=None):
    data = []
    while (max_len is None or len(data) < max_len) and len(stack_of_lists) > 0:
        data.extend(stack_of_lists.pop(0))
    return data


def split_random(data, num_train, num_val, num_test, random_seed=None):
    indices = list(range(len(data)))

    if random_seed is not None:
        np.random.seed(random_seed)
    np.random.shuffle(indices)

    train_indices = indices[: num_train]
    val_indices = indices[num_train : num_train + num_val]
    test_indices = indices[num_train + num_val :]

    train_data = [data[idx] for idx in train_indices]
    val_data = [data[idx] for idx in val_indices]
    test_data = [data[idx] for idx in test_indices]

    return train_data, val_data, test_data


def split_grouped(data, grouping, num_train, num_val, num_test):
    groups = {}
    for dd in data:
        group = "_".join(dd.split("_")[:grouping])
        if group not in groups:
            groups[group] = []
        groups[group].append(dd)

    data_grouped = sorted(list(groups.values()), key=lambda list_ : len(list_), reverse=True)

    train_data = pop_lists_until_max_len(data_grouped, num_train)
    val_data = pop_lists_until_max_len(data_grouped, num_val)
    test_data = pop_lists_until_max_len(data_grouped) # just the rest

    return train_data, val_data, test_data


def split(data, grouping=None, random_seed=None):
    train = 0.8
    val = 0.1

    dataset_size = len(data)
    num_train = int(np.floor(train * dataset_size))
    num_val = int(np.floor(val * dataset_size))
    num_test = dataset_size - num_train - num_val

    if grouping is not None:
        train_data, val_data, test_data = split_grouped(data, grouping, num_train, num_val, num_test)
    else:
        train_data, val_data, test_data = split_random(data, num_train, num_val, num_test)

    return train_data, val_data, test_data


def copy_data(source_dir, target_dir, filenames):
    progress = logging.ProgressPrinter("    Copying {num_files} files from '{source_dir}' to '{target_dir}' | File {step}/{max_step} ({percent:.1f}%) | ETA: {eta} | Total Time: {total_time}",
                                        max_step=len(filenames), log_step=1)
    progress.start(num_files=len(filenames), source_dir=source_dir, target_dir=target_dir)

    utils.make_dirs_checked(target_dir)
    for filename in filenames:
        src_file = os.path.join(source_dir, filename)
        dst_file = os.path.join(target_dir, filename)
        shutil.copyfile(src_file, dst_file)

        progress.step(num_files=len(filenames), source_dir=source_dir, target_dir=target_dir)

    progress.finish()


def copy_split(root_dir, train, val, test):
    all_data_dir = os.path.join(root_dir, 'all')
    copy_data(all_data_dir, os.path.join(root_dir, 'training'), train)
    copy_data(all_data_dir, os.path.join(root_dir, 'validation'), val)
    copy_data(all_data_dir, os.path.join(root_dir, 'test'), test)


def find_images(directory, ext=('png', 'jpg', 'jpeg')):
    """Finds all image files with the specified extenstion
        in the given directory.
    """
    return [img for img in os.listdir(directory) if img.endswith(ext)]


def parse_directory(directory):
    """Extracts all images from the directory and splits them into
        Democratic, Republican and Other according to the prefix
    """
    democratic = [] # label 1
    republican = [] # label 2
    other = [] # remaining labels
    for img in find_images(directory):
        party = img.split("_")[0]
        party = int(party) if party.isdigit() else None
        if party == 1:
            democratic.append(img)
        elif party == 2:
            republican.append(img)
        elif party is not None:
            other.append(img)
        # else no valid tag -> ignore
    return democratic, republican, other


def split_data_directory(root_dir, grouping=None, random_seed=None):
    all_data_dir = os.path.join(root_dir, 'all')

    democratic, republican, other = parse_directory(all_data_dir)

    logger.info("Copying democratic images ({})".format(len(democratic)))
    copy_split(root_dir, *split(democratic, grouping, random_seed))

    logger.info("Copying republican images ({})".format(len(republican)))
    copy_split(root_dir, *split(republican, grouping, random_seed))

    logger.info("Copying remaining images ({})".format(len(other)))
    copy_split(root_dir, *split(other, grouping, random_seed))



if __name__ == "__main__": # without that we get 'broken pipe' error with multiple workers
    logger.init_logfile('splitting data')
    logger.info("===============================================================")
    logger.info("Splitting data")
    logger.info("===============================================================")
    logger.info("")

    random_seed = 123456
    grouping = 4
    split_data_directory("../Data/wisc_images/Storyboards - Governor/", grouping, random_seed)
    split_data_directory("../Data/wisc_images/Storyboards - House/", grouping, random_seed)
    split_data_directory("../Data/wisc_images/Storyboards - Senate/", grouping, random_seed)