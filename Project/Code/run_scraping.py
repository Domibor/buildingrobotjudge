from utils import logging
import youtube_dl

logger = logging.get_logger()



def test():
    params = {
        'outtmpl' : "../Data/%(title)s.%(ext)s"
    }
    with youtube_dl.YoutubeDL(params) as ydl:
        ydl.download(['https://archive.org/details/FOXNEWSW_20190518_160000_Americas_News_HQ/start/60/end/120'])



if __name__ == "__main__": # without that we get 'broken pipe' error with multiple workers
    logger.init_logfile('scraping')
    logger.info("===============================================================")
    logger.info("Scraping")
    logger.info("===============================================================")
    logger.info("")
    
    test()