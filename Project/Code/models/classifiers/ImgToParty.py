'''
    simple image feature to party classifier
    VGG or similar as feature extraction
'''

import torch
import torch.nn as nn
import torchvision

# ========================================================================================================================

def freeze(model):
    for param in model.parameters():
        param.requires_grad = False

# ========================================================================================================================


class VGGBackBone(nn.Module):
    def __init__(self, config):
        super().__init__()

        self.config = config

        self.out_chn = 512

        if self.config.backbone == 'vgg11':
            vgg = torchvision.models.vgg11(pretrained=self.config.load_pretrained_backbone)
        elif self.config.backbone == 'vgg11_bn':
            vgg = torchvision.models.vgg11_bn(pretrained=self.config.load_pretrained_backbone)
        elif self.config.backbone == 'vgg13':
            vgg = torchvision.models.vgg13(pretrained=self.config.load_pretrained_backbone)
        elif self.config.backbone == 'vgg13_bn':
            vgg = torchvision.models.vgg13_bn(pretrained=self.config.load_pretrained_backbone)
        elif self.config.backbone == 'vgg16':
            vgg = torchvision.models.vgg16(pretrained=self.config.load_pretrained_backbone)
        elif self.config.backbone == 'vgg16_bn':
            vgg = torchvision.models.vgg16_bn(pretrained=self.config.load_pretrained_backbone)
        elif self.config.backbone == 'vgg19':
            vgg = torchvision.models.vgg19(pretrained=self.config.load_pretrained_backbone)
        elif self.config.backbone == 'vgg19_bn':
            vgg = torchvision.models.vgg19_bn(pretrained=self.config.load_pretrained_backbone)


        if not self.config.refine_backbone_feature_extractor:
            freeze(vgg)

        self.features = vgg.features

        # output dim 512 x 7 x 7 for input 224 x 224

        # self.avgpool = vgg.avgpool
        # self.classifier = vgg.classifier[:-1]

        # # output dim 4096

        # self.fc = nn.Sequential(
        #     nn.Linear(4096, 512),
        #     nn.ReLU(True),
        #     nn.Dropout()
        # )

        # # output dim 512

    def forward(self, x):
        x = self.features(x)
        # print(('features', x.shape))
        # x = self.avgpool(x)
        # print(('avgpool', x.shape))
        # x = x.view(x.size(0), -1)
        # print(('view', x.shape))
        # x = self.classifier(x)
        # print(('classifier', x.shape))

        # x = self.fc(x)
        # print(('fc', x.shape))
        return x

class ResnetBackBone(nn.Module):

    def __init__(self, config):
        super().__init__()

        self.config = config

        self.out_chn = 512
        if self.config.backbone == 'resnet18':
            resnet = torchvision.models.resnet18(pretrained=self.config.load_pretrained_backbone)
            self.out_chn = 512
        elif self.config.backbone == 'resnet34':
            resnet = torchvision.models.resnet34(pretrained=self.config.load_pretrained_backbone)
            self.out_chn = 512
        elif self.config.backbone == 'resnet50':
            resnet = torchvision.models.resnet50(pretrained=self.config.load_pretrained_backbone)
            self.out_chn = 2048
        elif self.config.backbone == 'resnet101':
            resnet = torchvision.models.resnet101(pretrained=self.config.load_pretrained_backbone)
            self.out_chn = 2048
        elif self.config.backbone == 'resnet152':
            resnet = torchvision.models.resnet152(pretrained=self.config.load_pretrained_backbone)
            self.out_chn = 2048

        if not self.config.refine_backbone_feature_extractor:
            freeze(resnet)

        self.conv1 = resnet.conv1
        self.bn1 = resnet.bn1
        self.relu = resnet.relu
        self.maxpool = resnet.maxpool
        self.layer1 = resnet.layer1
        self.layer2 = resnet.layer2
        self.layer3 = resnet.layer3
        self.layer4 = resnet.layer4

        # output dim 512 (18, 34) or 2048 (50, 101, 152) x 7 x 7 for input 224 x 224

        # self.avgpool = resnet.avgpool

        # # output dim 512 (18, 34), 2048 (50, 101, 152)

        # self.fc = nn.Sequential(
        #     nn.Linear(self.out_chn, 512),
        #     nn.ReLU(True),
        #     nn.Dropout()
        # )

        # # output dim 512

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        # print(('layer1', x.shape))
        x = self.layer2(x)
        # print(('layer2', x.shape))
        x = self.layer3(x)
        # print(('layer3', x.shape))
        x = self.layer4(x)
        # print(('layer4', x.shape))

        # x = self.avgpool(x)
        # print(('avgpool', x.shape))

        # x = x.view(x.size(0), -1)
        # print(('view', x.shape))

        # x = self.fc(x)
        # print(('fc', x.shape))

        return x

# ========================================================================================================================

class ImageParty(nn.Module):
    '''
        Base class for models.
    '''
    def __init__(self, config):
        super().__init__()

        self.config = config

        if 'vgg' in self.config.backbone:
            self.backbone = VGGBackBone(config)
        elif 'resnet' in self.config.backbone:
            self.backbone = ResnetBackBone(config)
        else:
            raise KeyError('invalid backbone')


        # print(self.backbone)
        # backbone.out_chn x N x N
        # maybe some more convolutions to bring backbone.out further down?
        self.avgpool = nn.AdaptiveAvgPool2d((7, 7))
        # backbone.out_chn x 7 x 7
        # self.classifier = nn.Sequential(
        #     nn.Linear(self.backbone.out_chn * 7 * 7, 512),
        #     nn.ReLU(True),
        #     nn.Dropout(),
        #     nn.Linear(512, 64),
        #     nn.ReLU(True),
        #     nn.Dropout(),
        #     nn.Linear(64, 2),
        # )
        self.classifier = nn.Sequential(
            nn.Linear(self.backbone.out_chn * 7 * 7, 512),
            nn.ReLU(True),
            nn.Dropout(),
            nn.Linear(512, 512),
            nn.ReLU(True),
            nn.Dropout(),
            nn.Linear(512, 2),
        )

    def forward(self, x):

        x = self.backbone(x)
        # print(('backbone', x.shape))

        x = self.avgpool(x)
        # print(('avgpool', x.shape))

        x = x.view(x.size(0), -1)
        # print(('view', x.shape))

        x = self.classifier(x)
        # print(('classifier', x.shape))

        return x


# ========================================================================================================================
