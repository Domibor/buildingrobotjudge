import torch
import time
import os
from . import logging

logger = logging.get_logger()

def save_learned_model(path, model, epoch, global_step, optimizer, scheduler):
    torch.save({
                'model_state_dict' : model.state_dict(),
                'epoch' : epoch,
                'global_step' : global_step,
                'optimizer_state_dict' : optimizer.state_dict(),
                'scheduler_state_dict' : scheduler.state_dict()
                #'loss': loss # could be something with current best, and only save model when better
                }, path)
    
def load_learned_model(path, device, model, optimizer=None, scheduler=None):
    checkpoint = torch.load(path, map_location=device)

    model.load_state_dict(checkpoint['model_state_dict'])

    start_epoch = checkpoint['epoch']
    global_step = checkpoint['global_step']

    if optimizer is not None:
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])

    if scheduler is not None:
        scheduler.load_state_dict(checkpoint['scheduler_state_dict'])
    
    return start_epoch, global_step

def get_checkpoint_path(model_dir, model_name, ckpt_to_load):    
    if ckpt_to_load == -1:
        # find latest checkpoint (= highest number in filename)
        import glob
        import re
        ckpt_path = os.path.join(model_dir, model_name)
        list_of_files = glob.glob(ckpt_path + "_*.pwf")

        def extract_number(f):
            s = re.findall("\d+$",f)
            return (int(s[0]) if s else -1,f)

        if len(list_of_files) > 0:
            ckpt_path = max(list_of_files, key=extract_number)
        else:
            ckpt_path = None
    else:
        ckpt_path = os.path.join(model_dir, "{}_{:03}.pwf".format(model_name, ckpt_to_load))

    return ckpt_path

def load_checkpoint(model_dir, model_name, ckpt_to_load, device, model, optimizer=None, scheduler=None, log_console=True):
    if log_console:
        t0_load_ckpt = time.time()
    ckpt_path = get_checkpoint_path(model_dir, model_name, ckpt_to_load)
    
    start_epoch, global_step = None, None
    if ckpt_path is not None and os.path.exists(ckpt_path):
        if log_console:
            logger.info("Loading checkpoint '{}' ...".format(ckpt_path), end="")
        start_epoch, global_step = load_learned_model(ckpt_path, device, model, optimizer, scheduler)    
        if log_console:
            logger.info("\b\b\b\b - {:.2f}sec".format(time.time() - t0_load_ckpt))
    else:
        if ckpt_path is None:
            logger.error("No checkpoint found in '{}'! ".format(model_dir), end="")
        else:
            logger.error("Checkpoint '{}' does not exist! ".format(ckpt_path), end="")
        logger.error("No parameters loaded.")
    return start_epoch, global_step

