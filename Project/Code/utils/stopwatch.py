# from torch import cuda
import time

class Stopwatch(object):
    def __init__(self, synchronize_cuda=False):
        '''
            Simple stopwatch for measureing time.
            synchronize_cuda: Cuda runs asynchronous and thus CPU should be synchronized
                with GPU when measuring time. 
        '''
        self.synchronize_cuda = synchronize_cuda
        self.t_start = time.time()

    def tic(self):
        if self.synchronize_cuda and cuda.is_available():
            cuda.synchronize()
        self.t_start = time.time()

    def toc(self, unit='sec'):
        '''
            Returns elapsed time since last tic() call.
            Possible units: ['sec' (default), 'ms']
        '''
        if self.synchronize_cuda and cuda.is_available():
            cuda.synchronize()
        elapsed_time = time.time() - self.t_start # seconds
        if unit == 'ms':
            elapsed_time *= 1000
        return elapsed_time

# ========================================================================

global_stopwatch = Stopwatch()

def tic(synchronize_cuda=False):
    '''
        Global, static method for measuring time.
        Starts stopwatch.
    '''
    global global_stopwatch
    global_stopwatch.synchronize_cuda = synchronize_cuda
    global_stopwatch.tic()

def toc(unit='sec'):
    '''
        Global, static method for measuring time.
        Stops stopwatch and returns elapsed time.
    '''
    return global_stopwatch.toc(unit)
