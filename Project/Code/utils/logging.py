from utils import utils, stopwatch
import datetime
import logging
import colorlog
import math

# ========================================================================================================================

class Logger():
    __logger = None

    class __Logger():
        def __init__(self):

            # the logger to use
            self.__logger = colorlog.getLogger()

            # the format for logging to console
            console_formatter = colorlog.ColoredFormatter(
                '%(log_color)s%(message)s',
                log_colors={
                    'DEBUG':    'bold_cyan',
                    'INFO':     'bold_green',
                    'WARNING':  'bold_yellow',
                    'ERROR':    'bold_red',
                    'CRITICAL': 'white,bg_red',
                }
                )
            self.__console_handler = logging.StreamHandler()
            self.__console_handler.setFormatter(console_formatter)
            self.__logger.addHandler(self.__console_handler)

            # matplotlib adds annoying debug logs as soon as it is imported -> disable logging for matplotlib
            mpl_logger = logging.getLogger('matplotlib')
            mpl_logger.setLevel(logging.WARNING)

            # the logging level to consider
            self.__logger.setLevel(logging.DEBUG)

        def init_logfile(self, output_file):
            # the format for logging to file
            utils.make_dirs_checked("../Log/")
            file_formatter = logging.Formatter("%(asctime)s [%(threadName)s] [%(levelname)s] %(message)s")
            file_handler = logging.FileHandler("../Log/{}_{}.log".format(datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S'), output_file))
            file_handler.setFormatter(file_formatter)
            self.__logger.addHandler(file_handler)

        def set_level(self, level):
            self.__logger.setLevel(level)

        def debug(self, msg, end="\n"):
            self.__console_handler.terminator = end
            self.__logger.debug(msg)

        def info(self, msg, end="\n"):
            self.__console_handler.terminator = end
            self.__logger.info(msg)

        def warning(self, msg, end="\n"):
            self.__console_handler.terminator = end
            self.__logger.warning(msg)

        def error(self, msg, end="\n"):
            self.__console_handler.terminator = end
            self.__logger.error(msg)

        def critical(self, msg, end="\n"):
            self.__console_handler.terminator = end
            self.__logger.critical(msg)

    @staticmethod
    def init_logfile(output_file="Log"):
        Logger.get_logger().init_logfile(output_file)

    @staticmethod
    def get_logger():
        if Logger.__logger is None:
            Logger.__logger = Logger.__Logger()
        return Logger.__logger


def init_logfile(output_file):
    return Logger.init_logfile(output_file)

def get_logger():
    return Logger.get_logger()

# ========================================================================================================================

logger = get_logger()

# ========================================================================================================================

class AverageMeter(object):
    def __init__(self):
        self.__total = None
        self.__counter = 0

    def update(self, value):
        if self.__total is None:
            self.__total = value
        else:
            self.__total += value
        self.__counter += 1

    @property
    def average(self):
        return self.__total / self.__counter if self.__counter > 0 else float("inf")

class AverageMeterDict(object):
    def __init__(self):
        self.__total = {}

    def update(self, dict_value):
        for key, value in dict_value.items():
            if isinstance(value, list) or isinstance(value, dict): # nested containers not supported
                continue

            if key not in self.__total:
                self.__total[key] = AverageMeter()
            self.__total[key].update(value)

    @property
    def average(self):
        avg = {}
        for key, value in self.__total.items():
            avg[key] = value.average
        return avg

# ========================================================================================================================
# simple class to print something on start and then print something together with execution time on stop

class ProgressPrinter(object):
    '''
        Reserved text formatting keywords for internal data:
            step : the current step index
            max_step : the maximum number of steps
            percent : 100 * step / max_step
            eta : expected time to finish, average_step_time * remaining_items
            total_time : total running time since start
    '''
    def __init__(self, text, max_step, log_step=1):
        self.__text = text
        self.__max_step = max_step
        self.__current_step = 0
        self.__log_step = log_step
        self.__stopwatch_total = stopwatch.Stopwatch()
        self.__stopwatch_step = stopwatch.Stopwatch()
        self.__average_step_time = AverageMeter()

    def start(self, *args, **kwargs):
        self.__stopwatch_total.tic()
        self.__stopwatch_step.tic()
        self.__print(*args, **kwargs)

    def step(self, *args, **kwargs):
        self.__average_step_time.update(self.__stopwatch_step.toc())
        self.__stopwatch_step.tic()
        self.__current_step += 1
        if self.__current_step % self.__log_step == 0 or self.__current_step >= self.__max_step:
            self.__print(*args, **kwargs)

    def finish(self):
        logger.info("") # simply new line

    def pause(self):
        pass

    def resume(self):
        pass

    def __print(self, *args, **kwargs):
        eta = (self.__max_step - self.__current_step) * self.__average_step_time.average
        if eta != float('inf'):
            eta = 0 if math.isnan(eta) else eta
            eta = str(datetime.timedelta(seconds = int(math.ceil(eta))))
        progress_data = {
            'step' : self.__current_step,
            'max_step' : self.__max_step,
            'percent' : 100 * (self.__current_step / self.__max_step if self.__max_step > 0 else 1),
            'eta' : eta,
            'total_time' : str(datetime.timedelta(seconds = int(self.__stopwatch_total.toc())))
        }
        # clear last line ('\033[K') and overwrite with new progress
        logger.info('\r' + '\033[K' + self.__text.format(*args, **kwargs, **progress_data), end='')