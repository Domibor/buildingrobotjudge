
from utils import utils, logging
from data import annotation as ann
import os
import cv2

logger = logging.get_logger()

def __process_directory(video_root_dir, party, year, image_root_dir, step_size, image_shape):
    video_directory = os.path.join(video_root_dir, party, str(year))

    video_files = [video_file for video_file in os.listdir(video_directory) if video_file.endswith('.mp4')]


    ''' read the video with step size '''
    for vid_idx, video_file in enumerate(video_files, 1):
        basename, _ = os.path.splitext(video_file)
        image_directory = os.path.join(image_root_dir, party, str(year), basename)
        utils.make_dirs_checked(image_directory)

        abs_path = os.path.join(video_directory, video_file)
        cap = cv2.VideoCapture(abs_path)
        
        prefix = "[{party}|{year} {idx}/{tota}]".format(idx=vid_idx, tota=len(video_files), party=party, year=year)
        print_text = prefix + " " + video_file + " | Frame {step}/{max_step} ({percent:.1f}%) | ETA: {eta} | Total Time: {total_time}"

        total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT ) // step_size)

        progress = logging.ProgressPrinter(print_text, max_step=total_frames)
        progress.start()

        frame_counter = 0
        while True:
            for _ in range(step_size - 1): # skip step_size - 1 frames
                valid_flush, frame_flush = cap.read()

            valid, frame = cap.read()
            if not valid:
                break
                

            # resize with padding
            old_size = frame.shape[:2] # old_size is in (height, width) format
            ratio = image_shape / max(old_size)
            new_size = (int(old_size[1] * ratio), int(old_size[0] * ratio)) # new_size should be in (width, height) format
            
            frame = cv2.resize(frame, new_size, interpolation = cv2.INTER_CUBIC)

            delta_w = image_shape - new_size[0]
            left = delta_w // 2
            right = delta_w - left
            delta_h = image_shape - new_size[1]
            top = delta_h // 2 
            bottom = delta_h - top

            color = [0, 0, 0]
            image = cv2.copyMakeBorder(frame, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)


            img_name = "{:06d}".format(frame_counter)
            img_filename = img_name + ".jpg"
            ann_filename = img_name + ".json"
            annotation = ann.Annotation(image_filename=img_filename, class_label=ann.get_class_label(party))

            ann_path = os.path.join(image_directory, ann_filename)
            annotation.save(ann_path)

            image_path = os.path.join(image_directory, img_filename)
            cv2.imwrite(image_path, image)

            progress.step()

            frame_counter += 1

        progress.finish()


def create_annotated_images(video_root_dir, parties, years, image_root_dir, step_size, image_shape):    

    for party in parties:
        logger.info("===============================")
        logger.info(party)
        logger.info("===============================")
        logger.info("")
        for year in years:
            logger.info(party + " | " + str(year))
            logger.info("")

            video_directory = os.path.join(video_root_dir, party, str(year))
            if os.path.exists(video_directory):
                __process_directory(video_root_dir, party, year, image_root_dir, step_size, image_shape)

            logger.info("-------------------------------")
            logger.info("")
